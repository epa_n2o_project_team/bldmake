EXEC = bldmake

# inherit from driver
# FC = /share/linux86_64/intel/fc/11.1.059/bin/intel64/ifort
# override driver
F_FLAGS = -O2 -fixed -extend_source -WB

OBJS = cfg_module.o parser.o utils.o bldmake.o

.SUFFIXES: .f

$(EXEC): $(OBJS)
	$(FC) $(OBJS) -o $@

.f.o:
	$(FC) -c $(F_FLAGS) $<

clean:
	rm config.cmaq *.log *.o *.mod $(EXEC)

